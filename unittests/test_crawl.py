"""
Unity tests

"""
import unittest
import os.path
import sys
import filecmp

from ltk import CrawlFiles

class SimpleConfigTestCase(unittest.TestCase):
    """
    Config base class, just let the __init__ to others TestCase
    """

    def setUp(self):
        self.mnemonic = "fakelog"
        self.test_file = "{}/test_files/a.txt".format(os.path.dirname(__file__))
        self.cf = CrawlFiles(self.mnemonic)

class DefaultConfigTestCase(SimpleConfigTestCase):
    """
    Test the module class public interface
    """

    def test_processFile(self):
        e, d, data = self.cf.processFile(self.test_file)
        self.assertEqual(e, 0)

    def test_generateLinks(self):
        e, d, data = self.cf.generateLinks(self.test_file)
        self.assertEqual(e, 0)

    def test_searchSum(self):
        self.cf.generateLinks(self.test_file)
        e, d, data = self.cf.searchSum(self.test_file)
        self.assertEqual(e, 0)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(DefaultConfigTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)

