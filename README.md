## Quick start

Quick start:

- $ sudo apt-get update
- $ sudo apt-get install python3.6
- $ sudo apt install python3-pip
- $ python3 ltk.py -f < full_path_to_first_file >


## Documentation

This project has 2 main python files:

ltk.py: with the core classes:

    - processFile: Load file data, numbers are treated as float to achieve float handling not only integers
            accumulates the local sum and generate a local list of link files

    - generateLinks: Search recursively all linked files through full path in files

    - searchSum: This Function will recursively search in deep the accumulative sum of the list

    - showResults: Format the List with json items, logs to a file and show as stdout as well

error.py: my private error handling code


## Test Files Structure

One integer per line

One full path to file per line

ie.

a.txt:
--------------
2

3

89

/home/ltk/test/b.txt


## File Structure

Within the project you will find the following directories and files:

```
Love to Know crawlfile
ltk
.
├── ltk.py
├── error.py
├── unittests/
|    ├──test_files
│    |    ├── e.txt
│    |    ├── d.txt
│    |    ├── c.txt
│    |    ├── b.txt
│    |    ├── a.txt
|    ├── test_crawl.py
├── README.md


