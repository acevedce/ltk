"""
Error coding and loggin module

Documentation syntax https://www.python.org/dev/peps/pep-0287/

:Date: 2018-09
:Version: 1.0
:Authors:
    - Cesar Acevedo

"""
import os
import sys
import getopt
import logging
import logging.handlers
from os.path import expanduser


class ErrorTools(object):
    """
    Error encoding/decoding tools and a logger wraper with the python native log handler
    """

    """
    Constant attributes
    ===================
    """

    _errorCode = {

        "interface": ("INTERFACE_OK, "
                      "INTERFACE_FAIL, "
                      "FILE_NOT_FOUND, "),

    }

    """for each _errorCode[group] the label shall be unique, which is validated in the __init__:method:
    """
    """
    Public getters & setters
    ========================
    """

    @property
    def errorCode(self):
        return self._errorCode

    """No setter, constant attribute 
    """

    @property
    def errorMsg(self):
        return self._errorMsg

    """No setter, a single initialization in __init__:method: 
    """

    @property
    def log_dir(self):
        return self._log_dir

    @log_dir.setter
    def log_dir(self, x):
        self._log_dir = x

    """No setter, a single initialization in __init__:method:
       log_dir could be in the environment, otherwise default values are taken
       In regular basis this is an private property, it is public just for testing purposes 
    """

    @property
    def logpath(self):
        return self._logpath

    @logpath.setter
    def logpath(self, value):
        self._logpath = value

    """
    Init and new
    ============
    """

    def __init__(self):
        """
        initialize object
        """
             
        # validates error labels are unique:
        union_set = set()
        for key, value in self._errorCode.items():
            prev_set = set(union_set)
            prev_len = len(union_set)
            group_set = set(value.replace(' ', '').split(','))
            group_set.discard("UNDEFINED_ERROR_CODE")
            union_set = prev_set | group_set
            if not len(union_set) == prev_len + len(group_set):
                raise ValueError('errorCode group {0} has repeated label(s): {1}'.format(key, prev_set & group_set))
        
        # initializes atributes:
        # just in case logpath is not overwrite to avoid error with ontheflylog
        self._logpath = "raw-error"
        # NEVER UNDEFINED_ERROR here
        self._errorMsg = {
            'INTERFACE_OK': 'Success',
            'INTERFACE_FAIL': 'Error', 
            'FILE_NOT_FOUND': 'File Not Found',
        }
        
        self.log_dir = "{}/ltk/logs".format(expanduser("~"))

        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

    """
    Private methods
    ===============
    """

    """
    Logger handling methdos
    ----------------------
    """

    def _checkLoggingOps(self, logger):
        """ 
        Set the logging options

        :parameters:
            - logger: native python loggin to wrap
        :return: void
        """

        debug_flag = '{0}/DEBUG.on'.format(self.log_dir)
        """ debug_flag eases a simple touch DEBUG.on to enable DEBUG logLevel 
        """

        try:
            opts, args = getopt.getopt(sys.argv[1:], 'l:', ["logLevel="])
        except getopt.GetoptError:
            if os.path.exists(debug_flag):
                logger.setLevel(logging.DEBUG) 
            else:
                logger.setLevel(logging.INFO) 
            return 
        for opt, logLevel in opts:
            if opt in ("-l", "--logLevel="):
                if logLevel.lower() == "debug":
                    logger.setLevel(logging.DEBUG) 
                if logLevel.lower() == "info":
                    logger.setLevel(logging.INFO) 
                if logLevel.lower() == "warning":
                    logger.setLevel(logging.WARNING) 
                if logLevel.lower() == "error":
                    logger.setLevel(logging.ERROR) 
                if logLevel.lower() == "critical":
                    logger.setLevel(logging.CRITICAL) 
                else:
                    return
        if os.path.exists(debug_flag):
            logger.setLevel(logging.DEBUG) 
        else:
            logger.setLevel(logging.INFO) 
        return          

    def _getLoggerName(self, filename):
        """ 
        Get the logger name from the filename without .py extension

        :parameters:
            - filename: python module to use as logger
        :return: logname
        """
        pos = filename.rfind('.py')
        if pos == (len(filename) - 3):
            logname = filename[:-3]
        else:
            logname = filename
        return logname

    """
    Public methods
    ==============
    """

    """
    Error handling methdos
    ----------------------
    """

    def getErrorCode(self, error_label):
        """ 
        Return error code from a label

        :parameters:
            - error_label: label, unique 
        :return: error code, if error_label does not exists then -1
        """
        mnemonic = self.logpath.replace('.log', '')
        try:
            if error_label in self.errorCode['interface']:
                return self.errorCode['interface'].replace(' ', '').split(',').index(error_label)
            else:
                ontheflylog = self.setupLogger(mnemonic)
                ontheflylog.critical('BUG: No code for label: {0}'.format(error_label))
                return -1
        except Exception as error:
            ontheflylog = self.setupLogger(mnemonic)
            ontheflylog.critical('BUG: No code for label: {0}'.format(error_label))

    def getErrorLabel(self, group, error_code):
        """ 
        Return error label from a code

        :parameters:
            - group: the error group is the project error subset
            - error_code: code in the error group
        :return: error label, or error message
        """
        if error_code < len(self.errorCode[group].replace(' ', '').split(',')):
            return self.S_OK(self.errorCode[group].replace(' ', '').split(',')[error_code])
        else:
            return self.S_ERROR('No error label for group {0}, code {1}'.format(group, error_code))

    @staticmethod
    def S_ERROR(messageString=''):
        """ 
        Return value on error condition

        :parameters:
            - messageString: error description
        :return: dictionary { 'OK' : False, 'Message' : str( messageString )  }
        """
        return { 'OK' : False, 'Message' : str(messageString)  }

    @staticmethod
    def S_OK(value=None):
        """ 
        Return value on success

        :parameters:
            - value: generic object to return
        :return: dictionary { 'OK' : True, 'Value' : value }
        """
        return { 'OK' : True, 'Value' : value }

    def prepareMessage(self, group, error_code, description='', data=''):
        """
        Returns description of the error code of a group, with additional description
        """
        try:
            result = self.getErrorLabel(group, error_code)
            if not result['OK']:
                raise
        except Exception as e:
            mnemonic = self.logpath.replace('.log', '')
            ontheflylog = self.setupLogger(mnemonic)
            description = 'No label for group {0} error code {1}'.format(group, error_code)
            ontheflylog.warning(description)
            return error_code, description, data

        label = result['Value']
        try:
            description = '{0}: {1}'.format(self.errorMsg[label], description)
        except Exception as e:
            mnemonic = self.logpath.replace('.log', '')
            ontheflylog = self.setupLogger(mnemonic)
            description = "{0}: {1}".format(self.errorMsg['NO_DESCRIPTION'], label)
            ontheflylog.warning(description)
        return error_code, description, data

    """
    Logger handling methdos
    ----------------------
    """

    def setupLogger(self, file_mnemonic):
        """ 
        Initializes a customized python loggin

        :parameters:
            - file_mnemonic: a seed to create the log file path
        :return: python logging object
        """
        name = self._getLoggerName(file_mnemonic)
        name = os.path.join(self.log_dir, name) 
        self.logpath = '{0}.log'.format(name)

        if file_mnemonic == 'stderr' or file_mnemonic == 'stdout':
            logger = logging.getLogger(file_mnemonic)
        else:
            logger = logging.getLogger(name)

        if logger.handlers:
            # if logger 'name' already exists:
            return logger

        if file_mnemonic == 'stderr':
            handler = logging.StreamHandler(stream=sys.stderr)
        elif file_mnemonic == 'stdout':
            handler = logging.StreamHandler(stream=sys.stdout)
        else:
            handler = logging.handlers.RotatingFileHandler(name + '.log', mode='a', maxBytes=1000000, backupCount=5)

        formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
        handler.setFormatter(formatter)
        self._checkLoggingOps(logger)
        logger.addHandler(handler)
        if not (file_mnemonic == 'stderr' or file_mnemonic == 'stdout'):
            self.logpath = '{0}.log'.format(name)
            os.chmod(self.logpath, 0o666)
        return logger

    def remove_log_handlers(self, logger):
        """ 
        Remove handlers list of a logger
        In regular basis this function is used for testing purposes

        :parameters:
            - logger: the one created in setupLogger:method:
        :return: void
        """
        handlers = logger.handlers[:]
        for handler in handlers:
            handler.close()
            logger.removeHandler(handler)

        return

