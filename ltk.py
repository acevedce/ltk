"""
Love to Know CrawlFiles module

Documentation syntax https://www.python.org/dev/peps/pep-0287/

:Date: 2020-02
:Version: 1.0
:Authors:
    - Cesar Acevedo

"""
# coding=utf-8
import sys, os
import json
import re
import platform
import argparse
from error import ErrorTools

class CrawlFiles(object):
    """CrwalFiles is a technical test for Love to Know recruiting processes,
    Search for global sum of linked files
    """    
    def __init__(self, log_mnemonic=None):
        """
        initialize object

        :parameters:
            - log_mnemonic: :string: to compose the logger and filename
        :raises: ValueError exception
        """
        
        self.error_tools = ErrorTools()

        if log_mnemonic is not None:
            self._logger = self.error_tools.setupLogger(log_mnemonic)
        else:
            self._logger = self.error_tools.setupLogger(os.path.basename(sys.modules['__main__'].__name__))
        
        self._files_path = {}


    """
    Public interface getters & setters
    ----------------------------------
    """

    @property
    def logger(self):
        return self._logger

    @property
    def filesPath(self):
        return self._files_path

    @filesPath.setter
    def filesPath(self, x):
        self._files_path = x


    def parseArgs(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-f", "--file", required=True, help="You need to specify the path to the initial file (ie. /home/me/first.txt)")
        opts = parser.parse_args()
        return opts

    def processFile(self, basefile):
        """Load file data, numbers are treated as float to achieve float handling not only integers
            accumulates the local sum and generate a local list of link files
        
        :param basefile: full path of the target file
        :type basefile: str
        :return: dictionary contains the error handling status
        :rtype: dict
        """        
        error_code, error_description, data = self.error_tools.prepareMessage('interface',
                                                    self.error_tools.getErrorCode('INTERFACE_OK'))
        if not os.path.isfile(basefile):
            error_code, error_description, data = self.error_tools.prepareMessage('interface',
                                                        self.error_tools.getErrorCode('FILE_NOT_FOUND'))
        elif (basefile not in self.filesPath):
            file_content = open(basefile).readlines()
            acc = 0
            r_files = []
            for l in file_content:
                if l.strip().isdigit():
                    acc = acc + float(l)
                else:
                    r_files.append(l.strip())
            data={"sum": acc,"link":r_files}
        else:
            data=self.filesPath[basefile]
        return error_code, error_description, data


    def generateLinks(self, basefile):
        """Search recursively all linked files through full path in files
        
        :param basefile: the full path of the target file
        :type basefile: str
        :return: dictionary contains the error handling status
        :rtype: dict
        """        
        error_code, error_description, data = self.error_tools.prepareMessage('interface',
                                                    self.error_tools.getErrorCode('INTERFACE_OK'))

        error_code, error_description, data = self.processFile(basefile)
        if error_code != 0:
            return error_code, error_description, data
        else:
            self.filesPath[basefile] = data
            if data['link'] == []:
                return error_code, error_description, self.filesPath
            else: 
                for l in data['link']:
                    self.generateLinks(l)
        return error_code, error_description, data


    def searchSum(self, basefile):
        """This Function will recursively search in deep the accumulative sum of the list
        
        :param basefile: the full path of the target file
        :type basefile: str
        :return: dictionary contains the error handling status
        :rtype: dict
        """        
        error_code, error_description, data = self.error_tools.prepareMessage('interface',
                                                    self.error_tools.getErrorCode('INTERFACE_OK'))

        if self.filesPath[basefile]['link'] == []:
            for i in self.filesPath:
                if basefile in self.filesPath[i]['link']:
                    self.filesPath[i]['sum'] = self.filesPath[i]['sum'] + self.filesPath[basefile]['sum']
                    self.filesPath[i]['link'].remove(basefile)
        else:
            for i in self.filesPath[basefile]['link']:
                self.searchSum(i)
        return error_code, error_description, data

    def showResults(self):
        """Format the List with json items, logs to a file and show as stdout as well
        """        
        self.logger.info(json.dumps(ltk.filesPath, indent=4, sort_keys=True))
        print(json.dumps(ltk.filesPath, indent=4, sort_keys=True))    




if __name__ == '__main__':
    """Main class, generate CrawlFiles class, gets initial full path of 1st file
        generates links, search links and show results.
    """    
    ltk = CrawlFiles("ltk")
    opts = ltk.parseArgs()
    ltk.generateLinks(opts.file)
    while ltk.filesPath[opts.file]['link'] != []:
        ltk.searchSum(opts.file)
    ltk.showResults()

        